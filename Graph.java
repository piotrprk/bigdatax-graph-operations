import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Scanner;


class Edge{
	String node_name;
	public int weight;
	
	public Edge(String node) {
		this.node_name = node;
		this.weight = 1;
	}
	public Edge(String node, int weight) {
		this.node_name = node;
		this.weight = weight;
	}	
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public String toString() {
		return node_name + "["+weight+"]";
	}
    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof Edge)) {
            return false;
        }
        Edge e = (Edge) o;
        return  Objects.equals(node_name, e.node_name) &&
                weight == e.weight;
    }

    @Override
    public int hashCode() {
        return Objects.hash(node_name, weight);
    }	
}
class Node {
	// node name
	String name;
	// keep edges in linked list
	public LinkedList<Edge> edges;
	
	public Node(String name) {
		this.name = name;
		edges = new LinkedList<>();
	}
	
	public String toString() {
		/*
		String result = new String();
		for(Edge e: this.edges) {
			result += e;
		}
		return result;
		*/
		return this.name;
	}
    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof Node)) {
            return false;
        }
        Node node = (Node) o;
        return  Objects.equals(name, node.name) &&
                Objects.equals(edges, node.edges);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, edges);
    }	
	
}
public class Graph {
	// store nodes in a hashmap
	// key == Node name
	HashMap<String, Node> nodes;
	// check values
	int check_nodes;
	int check_edges;
	// node name prefix
	String PREFIX = "";
	// are edges directed ?
	boolean IS_DIRECTED = false;
	
	public Graph() {
		nodes = new HashMap<>();			
	}
	public Graph(boolean directed) {
		nodes = new HashMap<>();
		this.IS_DIRECTED = directed;
	}	
	public String toString() {
		String result = "";
		for (String e:nodes.keySet()) {
			result += e + " => " + nodes.get(e);
			result += ", ";
		}
		return result;
	}
	// load csv data into structure
	// n_lines - set how much data to read
	// set n_lines to -1 to read whole file
	public void loadDataCSV(String file_name, int n_lines) {
		// CSV defaults
		String DEFAULT_SEPARATOR = ",";
		String DEFAULT_QUOTE = "\"";

		// get the data
		File f = new File(file_name);

		try (Scanner scanner = new Scanner(f)){	
			int k = 0; // count already red lines
			while(scanner.hasNextLine() && (k < n_lines || n_lines == -1)) {
				String[] tokens = scanner.nextLine().split("\n");
				// read line by line
				for(String i:tokens) {
					// remove quotes
					i = i.replace(DEFAULT_QUOTE, "");
					// split line
					String[] line = i.split(DEFAULT_SEPARATOR);	// get data by split
					// add data
					if (k > 0)
						this.addData(line);
					// add table header at first line
					else 
						this.addHeader(line);
						
					//					
				}	
				k++;
			}
		} catch (FileNotFoundException ex) {
			System.out.println(ex);
		}				
	}
	private void addData(String[] row) {
		// TODO
	}
	public void loadDataMIS(String file_name) {
		int n_lines = -1;
		this.loadDataMIS(file_name, n_lines);
	}
	// use n_lines == -1 to load all
	public void loadDataMIS(String file_name, int n_lines) {
		// get the data
		File f = new File(file_name);

		try (Scanner scanner = new Scanner(f)){	
			int k = 0; // count already red lines
			while(scanner.hasNextLine() && (k < n_lines || n_lines == -1)) {
				String[] tokens = scanner.nextLine().split("\n");
				// read line by line
				for(String i:tokens) {
					// split line
					String[] line = i.split("\\s");	// get data by split
					// add data
					if (k > 0) {
						// name the nodes
						String from = PREFIX + line[1];
						String to = PREFIX + line[2];
						// undirected !
						if( IS_DIRECTED)
							this.addEdgeDir(from, to);
						else
							this.addEdge(from, to);							
					}
					// add table header at first line
					else 
						this.addHeader(line);
					
					//					
				}	
				k++;
			}
		} catch (FileNotFoundException ex) {
			System.out.println(ex);
		}				
	
	}
	private void addHeader(String[] row) {
		this.check_nodes = Integer.valueOf(row[2]);
		this.check_edges = Integer.valueOf(row[3]);
	}

	public void addEdgeDir(String a, String b) {
		// add node if don't exists
		if(! this.nodes.containsKey(a)) {
			this.nodes.put(a, new Node(a));
		}
		if(! this.nodes.containsKey(b)) {
			this.nodes.put(b, new Node(b));
		}	
		// edge exists ?
		Node node = this.nodes.get(a);
		boolean found = false;
		
		// look for it
		// if found increment weight
		ListIterator<Edge> iterator = node.edges.listIterator(); 
		while (iterator.hasNext()) {
			Edge edge = iterator.next();
			if (edge.node_name.equals( b )) {
				found = true;
				// increment weight
				edge.weight++;
				break;
			} 
		}

		if (!found) {
			// set new edge
			Edge edge = new Edge(b);
			node.edges.add(edge);	
		}
	}
	public void addEdgeUdir(String a, String b) {
		this.addEdgeDir(a, b);
		this.addEdgeDir(b, a);
	}
	public void addEdge(String a, String b) {
		this.addEdgeUdir(a, b);
	}
	public void removeEdge(String a, String b) {
		if (this.nodes.containsKey(a) && this.nodes.containsKey(b)) {
			// iterate over all edges of a node a and b
			Edge to_remove = null;
			for (Edge e:this.nodes.get(a).edges) {
				if (e.node_name.equals(b)) {
					to_remove = e;
				}
			}
			this.nodes.get(a).edges.remove(to_remove);
			// the same for node B
			to_remove = null;
			for (Edge e:this.nodes.get(b).edges) {
				if (e.node_name.equals(a)) {
					to_remove = e;
				}
			}
			this.nodes.get(b).edges.remove(to_remove);
		}
	}
	// check graph loading for errors
	public void graphLoadingCheck() {
		System.out.println();
		System.out.println("check nodes: " + 
				this.check_nodes + " " +
				((this.check_nodes != this.nodes.size()) ? " ERROR ": " OK "));
		int edges_count = 0;
		for (Node n:this.nodes.values()) {
			for (@SuppressWarnings("unused") Edge e:n.edges) {
				edges_count ++;
			}
		}		
		if (!IS_DIRECTED)
			edges_count = edges_count/2;
		System.out.println("check edges: " + 
				this.check_edges + " " +
				((check_edges != edges_count) ? " ERROR ": " OK "));
		System.out.println();
	}
	public void printGraph() {
		for (String node:this.nodes.keySet()) {
			this.printNode(node);
		}
	}
	public void printNode(String node) {
		System.out.print(node + " => ");
		for (Edge e:this.nodes.get(node).edges) {
			System.out.print(e + ", ");
		}
		System.out.print("\n");
	}	
	
	public void printSingleNode(String node) {
		System.out.println();
		System.out.println(node + " count edges: " + this.nodes.get(node).edges.size());
		System.out.println(node + " => " + this.nodes.get(node).edges);
		System.out.println();		
	}	
	public boolean checkEdge(String a, String b) {
		Node node = this.nodes.get(a);
		boolean found = false;
		// look for it
		// if found increment weight
		ListIterator<Edge> iterator = node.edges.listIterator(); 
		while (iterator.hasNext()) {
			Edge edge = iterator.next();
			if (edge.node_name.equals( b )) {
				found = true;
				break;
			} 
		}
		return found;
	}
	
}
