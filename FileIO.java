import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class FileIO {
	File f;
	public ArrayList<String> data;
	
	public FileIO(String fileName) {
		this.f = new File(fileName);
		this.data = new ArrayList<>();
		this.readFile();
	}
	public FileIO(String fileName, String pattern) {
		this.f = new File(fileName);
		this.data = new ArrayList<>();
		this.readFile(pattern);
	}	
	public FileIO(String fileName, int n_lines) {
		this.f = new File(fileName);
		this.data = new ArrayList<>();
		String pattern = "\\n"; // read every line
		this.readFile(pattern, n_lines);
	}		
	private void readFile() {
		String pattern = "\\n";
		readFile(pattern);
	}
	private void readFile(String pattern){
		int n_lines = -1;
		readFile(pattern, n_lines);
	}
	private void readFile(String pattern, int n_lines){
		try (Scanner scanner = new Scanner(this.f)){	
			int k = 0;
			while(scanner.hasNextLine() && (k < n_lines || n_lines == -1)) {
				String[] tokens = scanner.nextLine().split(pattern);
				for(String i:tokens) {
					this.data.add(i);	
				}	
				// count lines if n_lines is set 
				if (n_lines != -1) 
					k++;
			}
		} catch (FileNotFoundException ex) {
			System.out.println(ex);
		}		
	}
}

class CSVFileIO extends FileIO {
	String DEFAULT_SEPARATOR = ",";
	String DEFAULT_QUOTE = "\"";
	// full data list
	ArrayList<String[]> data_array;
	
	public CSVFileIO(String fileName) {
		super(fileName);
		data_array = new ArrayList<>();
	}
	public CSVFileIO(String fileName, int n_lines) {
		super(fileName, n_lines);
		data_array = new ArrayList<>();		
	}	
	public ArrayList<String[]> readCSV() {
		for(String i:data) {
			i = i.replace(DEFAULT_QUOTE, "");
			String[] tokens = i.split(DEFAULT_SEPARATOR);	// get data by split
			data_array.add(tokens);
		}
		data.clear(); // free memory
		return data_array;
	}
}
