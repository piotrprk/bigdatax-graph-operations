import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Stack;


class ENode extends Node{
	// distance from start node
	int distance;
	// parent node in the graph
	ArrayList<String> parent;
	
	public ENode(Node node) {
		super(node.name);
		this.distance = Integer.MAX_VALUE;
		this.parent = new ArrayList<>();
	}
	
	public ENode(Node node, int dist, String parent) {
		super(node.name);
		this.edges = node.edges;
		this.distance = dist;
		this.parent = new ArrayList<>();
		if (parent != null)
			this.parent.add(parent);
	}	
	public String toString() {
		String result = super.toString();
		result += " d:" + this.distance + " p:" + this.parent; 
		return result;
	}
    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof ENode)) {
            return false;
        }
        ENode node = (ENode) o;
        return  Objects.equals(name, node.name) &&
                Objects.equals(edges, node.edges) &&
                Objects.equals(parent, node.parent) &&
                distance == node.distance;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, edges);
    }		
}
public class GraphTools extends Graph{
	public GraphTools() {
		
	}
	// extended BFS
	// results with Map of distances to start node 
	// and parent nodes saved
	// key = node name
	// key tool  to find shortest paths
	public LinkedHashMap<String, ENode> eBFS(String a, String b) {
		//escape if a don't exist
		if ( !this.nodes.containsKey(a) )
			return null;
		// start node
		ENode start = new ENode(nodes.get(a), 0, null);
		// visited nodes HashMap of ENodes to keep distance and parent
		// LinkedHashMap keeps array order !
		LinkedHashMap<String, ENode> visited = new LinkedHashMap<>(this.nodes.size());
		// Queue
		LinkedList<ENode> Q = new LinkedList<>();
		
		// add start to Q
		Q.add(start);
		// mark visited
		visited.put(a, start);
		
		// traverse
		while (!Q.isEmpty()) {
			ENode n = Q.poll();
			for (Edge e:n.edges) {
				// check if visited
				if(!visited.containsKey(e.node_name)) {
					int dist = n.distance + e.weight;
					
					// end if next node distance is > end node (b) distance
					if (visited.containsKey(b))
						if (dist > visited.get(b).distance) {
							return visited;
						}				
					// set the next node
					ENode next = new ENode(this.nodes.get(e.node_name), dist, n.name);					
					// add to queue
					Q.add(next);
					// add to path
					visited.put(e.node_name, next);
				} 
				else {
					// add parent if its distance < next node
					// next = Node(e)
					ENode next = visited.get(e.node_name);
					if (n.distance < next.distance) 
						next.parent.add(n.name);
				}
			}
		}
		return visited;
	}
	// get all possible shortest paths from BFS
	// use BFS and recurse to get the paths (all possible paths)
	public ArrayList< ArrayList<String>> getShortestPath(String a, String b){
		// do BFS
		LinkedHashMap<String, ENode> bfs_map = eBFS(a, b);
		
		// declare result array
		ArrayList< ArrayList<String>> ret = new ArrayList<>();		

		// recurse to build shortest paths
			ArrayList< String > path = new ArrayList<>();
			this.addParents(b, bfs_map, ret, path);			
		return ret;
	}
	// recurse function to get shortest paths
	private void addParents(String node_name, LinkedHashMap<String, ENode> node_map, ArrayList< ArrayList<String> > result, ArrayList<String> path) {
		// add node to shortest path
		path.add(0, node_name);
		ENode n = node_map.get(node_name);
		// cycle through all its parents
		// until start node found
		if (n != null) {
			if (n.parent.size() > 0) {
				for (String parent_name:n.parent) {	
					// recurse to build path
					addParents(parent_name, node_map, result, path);
				}
			} else {
				// if path finishes add to result
				result.add(new ArrayList<String>(path));
			}
			// must remove !
			path.remove(0);
		}
	}
	// calculate shortest paths for all node pairs
	// path A-B is the same as B-A			
	private HashMap< String, HashMap<String, ArrayList< ArrayList<String>>> > calculateShortestPaths(){
		// structure to keep resolved pair paths
		HashMap< String, HashMap<String, ArrayList< ArrayList<String>>> > pairs_paths = new HashMap<>();
		// get list of nodes
		LinkedList<String> Q = new LinkedList<String>(this.nodes.keySet());
		// loop to find shortest paths
		for (int i=0; i<Q.size(); i++) {
			String a = Q.get(i);
			HashMap<String, ArrayList< ArrayList<String> >> map = new HashMap<>();
			for(int j=i+1; j<Q.size(); j++) {				
				String b = Q.get(j);
				ArrayList< ArrayList<String> > shortest_paths = this.getShortestPath(a, b);				
				map.put(b, shortest_paths);
			}
			pairs_paths.put(a, map);	
		}
		return pairs_paths;
	}
	public LinkedHashMap<String, Double> nodeBetweenness(){
		LinkedHashMap<String, Double> result = new LinkedHashMap<>();
		// get shortest paths
		HashMap< String, HashMap<String, ArrayList< ArrayList<String>>> > pairs_paths = this.calculateShortestPaths();
		//
		LinkedList<String> Q = new LinkedList<String>(this.nodes.keySet());
		// for each node v
		for (String v:Q) {
			// calculate nst/Nst for each pair
			Double betweenness = 0.0;
			for (int i=0; i<Q.size(); i++) {
				String a = Q.get(i);
				if (!a.equals(v)) {
					for(int j=i+1; j<Q.size(); j++) {				
						String b = Q.get(j);
						if (!b.equals(v)) {
							// 	number of shortest paths a-b, a!=v && b!=v
							int Nab = pairs_paths.get(a).get(b).size();
							// number of shortest paths a-b that goes through v
							int nab = 0;
							for(ArrayList<String> l:pairs_paths.get(a).get(b)) {
								if (l.contains(v))
									nab ++;
							}
							// sum the betweenness
							betweenness += (double)nab / (double)Nab;
						}
					}
				}
			}
			
			result.put(v, betweenness);
		}
		return result;
	}
	public LinkedHashMap<String, LinkedHashMap< String, Double> > edgeBetweenness(){
		// get shortest paths
		HashMap< String, HashMap<String, ArrayList< ArrayList<String>>> > pairs_paths = this.calculateShortestPaths();
		//
		LinkedList<String> Q = new LinkedList<String>(this.nodes.keySet());
		// result table
		/*
		double[][] result = new double[Q.size()][Q.size()];
		// calculate edge betweenness for all edges
		for (int i=0; i<Q.size(); i++) {
			String a = Q.get(i);
			for(int j=i+1; j<Q.size(); j++) {				
				String b = Q.get(j);				
				//
				// get number of paths
				int sp_num = pairs_paths.get(a).get(b).size();
				// iterate through shortest paths
				for(ArrayList<String> l:pairs_paths.get(a).get(b)) {
					for (int k=0; k<l.size()-1; k++) {
						String key_A = l.get(k);
						String key_B = l.get(k+1);
						// get node index in a list from Q
						// must keep list integrity to track results
						int index_A = Q.indexOf(key_A);
						int index_B = Q.indexOf(key_B);
						// must take into account number of shortest paths
						result[index_A][index_B] += (double)1/sp_num; 
						// edge AB is the same as BA						
						result[index_B][index_A] += (double)1/sp_num;
						//
					}
				}				
			}
		}
		LinkedHashMap<String, Double> result_map = new LinkedHashMap<>();
		for (int i=0; i<Q.size(); i++) {
			for (int j=0; j<Q.size(); j++) {
				// set key to string nodeA + nodeB
				String key = Q.get(i) + Q.get(j);
				if (result[i][j] > 0)
					result_map.put(key, result[i][j]);
			}
		}
		*/
		//
		LinkedHashMap<String, LinkedHashMap< String, Double> > result_double_map = new LinkedHashMap<>(Q.size());
		for(String A:pairs_paths.keySet()) {
			for (String B:pairs_paths.get(A).keySet()) {
				// get number of paths
				int sp_num = pairs_paths.get(A).get(B).size();
				// find all node pairs in a path
				for(ArrayList<String> l:pairs_paths.get(A).get(B)) {
					for (int k=0; k<l.size()-1; k++) {
						String key_A = l.get(k); // edge from
						String key_B = l.get(k+1); // edge to
						// put result in the map
						
						if (!result_double_map.containsKey(key_A)){
							result_double_map.put(key_A, new LinkedHashMap <String, Double>());
						}		
						if (!result_double_map.containsKey(key_B)){
							result_double_map.put(key_B, new LinkedHashMap <String, Double>());
						}						
						LinkedHashMap <String, Double> map = result_double_map.get(key_A);
						if (!map.containsKey(key_B))
							map.put(key_B, (double)1/sp_num);
						else {
							double val = map.get(key_B);
							val +=  (double)1/sp_num;
							map.put(key_B, val);
						}
						result_double_map.put(key_A, map);
						// the same for node B (key_B)
						map = result_double_map.get(key_B);
						if (!map.containsKey(key_A))
							map.put(key_A, (double)1/sp_num);
						else {
							double val = map.get(key_A);
							val +=  (double)1/sp_num;
							map.put(key_A, val);
						}
						result_double_map.put(key_B, map);						
					}
				}
			}
			
		}
		
		return result_double_map;
	}	
	// Breath first search
	public ArrayList<String> BFS(String a, String b) {
		Node start = this.nodes.get(a);
		// end node
		Node end = this.nodes.get(b);
		// visited nodes list
		ArrayList<String> visited = new ArrayList<>(this.nodes.size());
		// Queue
		LinkedList<Node> Q = new LinkedList<>();
		// track parent nodes
		LinkedList<String> prev = new LinkedList<>();
		// add start to Q
		Q.add(start);
		// mark visited
		visited.add(start.name);

		boolean found = false;
		// traverse
		while (!Q.isEmpty()) {
			Node n = Q.poll();
			for (Edge e:n.edges) {
				// check if visited
				if(!visited.contains(e.node_name)) {
					// get the node
					Node next = this.nodes.get(e.node_name);
					// add to queue
					Q.add(next);
					// mark visited
					visited.add(next.name);
					// check for end node
					if (next.equals(end)) {
						found = true;
						break;
					}
				}
			}
			if (found)
				break;
		}
		return visited;
	}
	public ArrayList<String> BFS(String a) {
		return BFS(a, null);
	}	
	// Depth first search
	public ArrayList<String> DFS(String a, String b) {
		Node start = this.nodes.get(a);
		// end node
		Node end = this.nodes.get(b);
		// visited nodes list
		ArrayList<String> visited = new ArrayList<>();
		// use Stack
		Stack<Node> S = new Stack<>();
		// add start to Stack
		S.push(start);
		//
		boolean found = false;
		// traverse
		while (!S.isEmpty()) {
			// get node from stack
			Node n = S.pop();
			// check if e is visited
			if(!visited.contains(n.name)) {
				// mark visited
				visited.add(n.name);				
				// traverse edges
				for (Edge e:n.edges) {
					// get the next node
					Node next = this.nodes.get(e.node_name);
					// add to queue
					S.push(next);
					// check for end node
					if (next.equals(end)) {
						found = true;
						// add last node to visited
						visited.add(e.node_name);
						break;
					}
				}
			}
			if (found)
				break;
		}
		return visited;		
	}
	public ArrayList<String> DFS(String a) {
		return DFS(a, null);	
	}
	// calculate MST with Prims
	// declare start node
	public ArrayList<String> PrimsMST(String first) {
		// calculate this
		int mst = 0;
		ArrayList<String> traverse_path = new ArrayList<>();
		
		// select first node arbitrary
		Node first_node = this.nodes.get(first);
		// 1. initialise Supernode as List of Nodes
		LinkedList<Node> S = new LinkedList<>();
		// add first node to S
		S.add(first_node);
		traverse_path.add(first);
		// Initialise list of all nodes
		LinkedList<Node> N = new LinkedList<>(this.nodes.values());		
		//remove first from N
		N.remove(first_node);
		
		// traverse nodes until all nodes are inside S
		while (S.size() != this.nodes.size()) {
			// find next node with smallest cost			
			// set min cost to infinity
			int min_cost = Integer.MAX_VALUE;
			// who's next ?
			String next_node_name = null;
			
			// check every node inside S
			for (Node i:S) {
				// calculate cost s-n for every node outside of S
				for(Node n:N) {
					// find edge with n -> i
					for(Edge e:n.edges) {
						if (e.node_name.equals(i.name)) {
							// check if it's of min weight
							if(e.weight < min_cost) {
								min_cost = e.weight;
								next_node_name = n.name;
							}
						}
					}

				}
			}
			// break if nothing's found
			if (next_node_name == null)
				break;
			Node next = this.nodes.get(next_node_name);
			// add node to S and remove from N
			if (N.remove(next))
				S.add(next);
			//add edge weight to MST
			mst += min_cost;
			//add to path
			traverse_path.add(next.name);
		}
		return traverse_path;
	}
	
	// Dijkstra's traversal algorithm 
	public ArrayList<String> DiSP(String a){
		return this.DiSP(a, null);
	}	
	public ArrayList<String> DiSP(String a, String b){

		// visited nodes list
		ArrayList<String> path = new ArrayList<>(this.nodes.size());
		// create Queue / unvisited
		// add all to Q
		LinkedList<String> Q = new LinkedList<>(this.nodes.keySet());

		// distance map u -> v for every node 
		HashMap<String, Integer> dist = new HashMap<>(Q.size());
		for (String n:Q) {
			dist.put(n, Integer.MAX_VALUE);
		}
		// set dist[start] to 0		
		dist.put(a, 0);
		//		
		// get start node
		int index = Q.indexOf(a);
		// mark visited / add to path
		path.add(a);
		
		// traverse	Q
		while (!Q.isEmpty()) {
			// get u as a node by index
			Node u = this.nodes.get( Q.get(index) );					
			// for every neighbour of u
			for(Edge e:u.edges) {
				// get node
				Node n = this.nodes.get(e.node_name);
				if (Q.contains(n.name)) {
					// set min dist u -> n
					int t = dist.get(u.name) + e.weight;
					if ( t < dist.get(n.name))
						dist.put(n.name, t); // set only if smaller					
				}
			}
			// remove u
			Q.remove(index);

			// find minimum dist node
			int min = Integer.MAX_VALUE;
			String min_dist_node = null;
			// 
			for(String k:Q) {
				if(dist.get(k) < min) {
					min = dist.get(k);
					min_dist_node = k;
				}
			}
			if (min_dist_node != null) {
				// next node is min_dist_node
				index = Q.indexOf(min_dist_node);
				// add next to path
				path.add(min_dist_node);
			}
			// terminate if end found
			if ( b != null)
				if (min_dist_node.equals(b))
					break;
		}
		return path;		
	}
	// set level of clustering
	public void GirvanNewman(int level){
		LinkedHashMap<String, LinkedHashMap< String, Double> > edge_bness = this.edgeBetweenness();
		// find edge with max Betweenness
		for (int i=0; i < level; i++) {
			double max = 0.0;
			String node_from = null;
			String node_to = null;
			for (String A:edge_bness.keySet()) {
				for (String B:edge_bness.keySet()) {
					if (edge_bness.containsKey(A)) {
						if (edge_bness.get(A).containsKey(B)) {
							double val = edge_bness.get(A).get(B);
							if (val > max) {
								max = val;
								node_from = A;
								node_to = B;						
							}
						}
					}
				}
			}
			System.out.println("max betweenness: " + node_from + node_to + " : " + max+ " - remove edge");
			// remove max betweenness edge		
			this.removeEdge(node_from, node_to);
			edge_bness.get(node_from).remove(node_to);
			edge_bness.get(node_to).remove(node_from);
		}
		//
	}
}
