import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class A2 {
	public static void printList(ArrayList<String> list, String title) {
		System.out.println();		
		System.out.println(title);		
		for (String s:list) {
			System.out.print(s);
			if (list.indexOf(s) != (list.size()-1))
				System.out.print(" -> ");
		}
		System.out.print("\n");
		System.out.println("distance: " + (list.size()-1));
	}	
	public static void main(String[] args) {

		// load file
		//String file_name = "ca-netscience.mis";
		String file_name = "question-1.mis";
		
		GraphTools graph = new GraphTools();
		graph.loadDataMIS(file_name);
		// print graph
		graph.printGraph();

		//check 
		graph.graphLoadingCheck();
		// single
		//graph.printSingleNode("N-333");
		//graph.printSingleNode("A");
		
		// MST
		//graph.PrimsMST("N-A");
		
		// get
		//String from = "N-333";
		//String to = "N-300";
		String from = "A";
		String to = "F";
		
		ArrayList<String> list = graph.BFS(from, to);
		printList(list, "BFS " + from + " -> " + to);
		// get
		list = graph.DFS(from, to);
		printList(list, "DFS " + from + " -> " + to);
		
		// Dijkstra's
		list = graph.DiSP(from, to);
		printList(list, "DiSP");
		
		// Prims
		list = graph.DiSP(from, to);
		printList(list, "Prims " + from + " -> " + to);
		
		// BFS - DFS
		String name = "A";
		System.out.println();						
		list = graph.BFS(name);
		printList(list, "All graph BFS: ");
		list = graph.DFS(name);
		printList(list, "All graph DFS: ");
		list = graph.DiSP(name);
		printList(list, "All graph DiSP: ");
		list = graph.PrimsMST(name);
		printList(list, "All graph Prims: ");

		// eBFS
		System.out.println();	
		System.out.println("====================");	
		to = "5";
		LinkedHashMap<String, ENode> elist = graph.eBFS(from, to);
		ArrayList<String> ret = new ArrayList<>();
		ArrayList<String> ret2 = new ArrayList<>();
		ArrayList<String> ret3 = new ArrayList<>();
		for (ENode n:elist.values()) {
			ret.add(n.name);
			ret2.add(String.valueOf(n.distance));
			ret3.add(n.parent.toString());
		}
		printList(ret, "eBFS " + from + " -> " + to);
		printList(ret2, " dist " + from + " -> " + to);
		printList(ret3, " parents " + from + " -> " + to);
		// find shortest path
		ArrayList< ArrayList<String>> path = graph.getShortestPath(from, to);
		for (ArrayList<String> l:path) {
			printList(l, "shortest path " + from + " -> " + to);
		}
		// calculate betweenness
		
		LinkedHashMap<String, Double> b_ness = graph.nodeBetweenness();
		System.out.println("====================");
		System.out.println("Node Betweenness");	
		for (Map.Entry<String, Double> set:b_ness.entrySet()) {
			System.out.println(set.getKey() + " : " + set.getValue());
		}
		System.out.println();
		LinkedHashMap<String, LinkedHashMap< String, Double> > eb_ness = graph.edgeBetweenness();
		System.out.println("Edge Betweenness");
		for (String A:eb_ness.keySet()) {
			for (String B:eb_ness.get(A).keySet()) {
				System.out.println(A + B + " : " + eb_ness.get(A).get(B));
			}
		}
		System.out.println();
		
		/*	
		graph.printGraph();
		graph.GirvanNewman(1);
		graph.printGraph();
		from = "1";
		list = graph.BFS(from);
		printList(list, "BFS path " + from + " -> ");
		from = "H";
		list = graph.BFS(from);
		printList(list, "BFS path " + from + " -> ");		
		*/
	} // MAIN

}
